from pyzabbix import ZabbixAPI
from random import choices
from datetime import datetime

z = ZabbixAPI('http://msk-dpro-app351')
z.login("andrey.gavrilov", "200994")

log_sender = z.do_request('item.get', {
    'filter': {'name': 'Log_sender using'},
    'searchWildcardsEnabled': 'true',
    'selectHosts': ['name'],
    'output': ['lastvalue', 'hosts']
})['result']
in_temp = z.do_request('host.get', {
    'templateids':['93211'],
    'output': ['host']
})['result']
print(len(in_temp))
pp = [i['hostid'] for i in in_temp]
poses = [{'hostid': i['hosts'][0]['hostid']} for i in log_sender if i['lastvalue'] == '1' and i['hosts'][0]['hostid'] not in pp]
poses = poses[:1000]


templates = [{
                "templateid": "93211"
            }]


print(poses[0])
add_poses = z.do_request('template.massadd', {
                          "templates": templates,
                          "hosts": poses
                })['result']
print(add_poses)
with open(str(datetime.now()).split('.')[0] + '.log', 'w') as f:
    for i in poses:
        f.write(i['hostid'] + '\n')