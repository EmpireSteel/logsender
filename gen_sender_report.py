# coding=utf-8
from zapi import ZabbixApi
from datetime import datetime
from send_mail import send_report
import sys


def get_data(item):
    z = ZabbixApi('http://msk-dpro-app351', 'andrey.gavrilov', '200994')
    find = z.zabbix_api_request(method='item.get', params={
        'filter': {'name': item},
        'templateid':133936,
        'searchWildcardsEnabled': 'true',
        'selectHosts': ['name'],
        'output': ['hosts']
    })
    data = {}
    count = 0
    for i in find:
        data[i['hosts'][0]['name']] = {'itemid': i['itemid']}
        history = z.zabbix_api_request(method='history.get', params={
            'itemids': data[i['hosts'][0]['name']]['itemid'],
            'history': 3,
            'output': 'extend',
        })
        if len(history) == 0:
            data[i['hosts'][0]['name']][item.split(' ')[0]] = '0'
        for his in history:
            if int(his['value']) > 0:
                data[i['hosts'][0]['name']][item.split(' ')[0]] = '1'
                count += 1
                break
            data[i['hosts'][0]['name']][item.split(' ')[0]] = '0'
    z.logout()
    with open('reports/' + item.split(' ')[0] + '_report.csv', 'w') as f:
        f.write(';'.join(['POS', item.split(' ')[0]]) + '\n')
        for i in data:
            f.write(';'.join([i, data[i][item.split(' ')[0]]]) + '\n')
    msg = "Total {0}: {1}\n".format(item.split(' ')[0], count)
    send_data(item, msg)


def send_data(item, msg):
    for recipient in sys.argv[1:]:
        send_report(subject='Отчет по кассам на {0}.'.format(item.split(' ')[0]),
                    text=msg,
                    to=recipient,
                    fname='reports/' + item.split(' ')[0] + '_report.csv')


if __name__ == '__main__':
    items = ['Filebeat using', 'Log_sender using']
    for i in items:
        get_data(i)




