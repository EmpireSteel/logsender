from pyzabbix import ZabbixAPI
from datetime import datetime

z = ZabbixAPI('http://msk-dpro-app351')
z.login("andrey.gavrilov", "200994")

def get_data(item):

    find = z.do_request('item.get', {
        'filter': {'name': item},
        'templateid':133936,
        'searchWildcardsEnabled': 'true',
        'selectHosts': ['name'],
        'output': ['hosts'],
    })['result']
    data = {}
    count = 0
    for i in find:
        data[i['hosts'][0]['name']] = {'itemid': i['itemid']}
        history = z.do_request('history.get', {
            'itemids': data[i['hosts'][0]['name']]['itemid'],
            'history': 3,
            'output': 'extend',
        })['result']
        if len(history) == 0:
            data[i['hosts'][0]['name']][item.split(' ')[0]] = '0'
        for his in history:
            if int(his['value']) > 0:
                data[i['hosts'][0]['name']][item.split(' ')[0]] = '1'
                count += 1
                break
            data[i['hosts'][0]['name']][item.split(' ')[0]] = '0'
    with open('reports/' + item.split(' ')[0] + '_report.csv', 'w') as f:
        f.write(';'.join(['POS', item.split(' ')[0]]) + '\n')
        for i in data:
            f.write(';'.join([i, data[i][item.split(' ')[0]]]) + '\n')
    msg = "Total {0}: {1}\n".format(item.split(' ')[0], count)
    print(msg)


if __name__ == '__main__':

    items = ['Filebeat using', 'Log_sender using']
    for i in items:
        get_data(i)




